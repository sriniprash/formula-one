/**
 * Created by Prashanth Sivasankaran on 4/28/17.
 */

public class FormulaOne {

    public FormulaOne() {

    }

    // Data structures

    float[] topSpeed,acceleration,distance, currentSpeed, currentTime, finishingTime;
    boolean[] raceCompleted,nitroUsed;

    int numCars, time;
    float trackLen;


    public void computeFinalSpeedAndTime(int numCars, int trackLength) {
        time=0;
        this.numCars = numCars;
        trackLen = trackLength;

        topSpeed = new float[this.numCars];
        acceleration = new float[this.numCars];
        finishingTime = new float[this.numCars];

        distance = new float[this.numCars];
        nitroUsed = new boolean[this.numCars];
        currentSpeed = new float[this.numCars];
        currentTime = new float[this.numCars];
        raceCompleted = new boolean[this.numCars];

        for(int i = 0; i< this.numCars; i++) {
            raceCompleted[i]=false;
            nitroUsed[i]=false;
        }

        // Compute the start positions first
        computeStartPositions();

        // Init top speeds
        initializeTopSpeeds();

        // Init accelarations
        initializeAcclerations();


        // While loop to run every 2 seconds


        while (!raceComplete()) {
            System.out.println("Updating status at t=" + time);
            float distanceCovered;

            System.out.println("-------------------------");
            System.out.println("Updating all distances.");
            for(int i = 0; i< this.numCars; i++){
                distanceCovered = 2* currentSpeed[i];
                distance[i] = distance[i] + distanceCovered;
                System.out.println("New Distance of " + (i+1) + " th car is " + distance[i]);
            }

            System.out.println("-------------------------");
            System.out.println("Updating all speeds.");
            float tempSpeed;

            for(int i = 0; i< this.numCars; i++){
                tempSpeed = currentSpeed[i] + (2* acceleration[i]);

                if (tempSpeed<= topSpeed[i])
                {
                    currentSpeed[i]= tempSpeed;}
                System.out.println("New speed of " + (i+1) + " th car is " + currentSpeed[i]);
            }




            // check if any car is within 10 meters ??

            System.out.println("-------------------------");
            System.out.println("Checking to see if there is any car within 10 meters");
            for(int i = 0; i< this.numCars; i++) {
                System.out.println("Looking up for " + (i+1) + " th car.");
                if (checkCarWithin10Mts(i)) {
                    // reduce car speed
                    currentSpeed[i] = currentSpeed[i]*0.8f;
                    System.out.println("Reduced current car speed to " + currentSpeed[i]);
                    // break;
                }
            }
            System.out.println("Checking to see nitro required: ");
            for(int i = 0; i< this.numCars; i++){
                if (isDriverLast(i)){
                    if(!nitroUsed[i]) {
                        float double_speed;
                        double_speed = currentSpeed[i]*2.0f;

                        if (double_speed > topSpeed[i]) {
                            currentSpeed[i] = topSpeed[i];
                        } else {
                            currentSpeed[i] = double_speed;
                        }
                        nitroUsed[i]=true;
                    }
                }
            }
            // update stats
            updateCompletionStats();
            time=time+2;
        }

        // Printing results
        System.out.println("Race complete in "+ time + " secs");
        System.out.println("Final Speeds: ");
        for(int i = 0; i< this.numCars; i++)
            System.out.println("Speed of " + (i+1) + " th car is " + currentSpeed[i]);
        System.out.println("Race completion times: ");
        for(int i = 0; i< this.numCars; i++)
            System.out.println("Race completion of " + (i+1) + " th car is " + finishingTime[i]);

    }

    private void computeStartPositions() {
        distance[0]=0;
        for(int i = 1; i< numCars; i++){
            distance[i] = distance[i-1] - (200*(i));
        }

        if ( !(distance[numCars -1]>((-1)* trackLen)) ) {
            System.out.println("All cars cannot come on the track !");
            System.exit(1);
        }
    }

    private void initializeTopSpeeds() {
        for(int i = 0; i< numCars; i++){
            topSpeed[i] =  (150 + (10*(i+1)));
            currentSpeed[i]=0.0f;
        }
    }

    private void initializeAcclerations() {
        for(int i = 0; i< numCars; i++) acceleration[i] = (2*(i+1));
    }

    private boolean raceComplete() {
        for(int i = 0; i< numCars; i++){
            if (distance[i]< trackLen)
                return false;
        }
        return true;
    }


    private boolean checkCarWithin10Mts(int currentCarnumber){
        for(int i = 0; i< numCars; i++){
            if (i!=currentCarnumber) {
                float currentDifference = distance[currentCarnumber] - distance[i];
                float negDifference = (-1)*currentDifference;
                if ( currentDifference < 10 && currentDifference>0){
                    System.out.println("car found within 10 meters.");
                    System.out.println("Current car position: " + distance[currentCarnumber] + ". Position of car "+ (i+1) + " within 10 mts : " + distance[i]);
                    return true;
                } else if (negDifference < 10 && negDifference>0) {
                    System.out.println("car found within 10 meters.");
                    System.out.println("Current car position: " + distance[currentCarnumber] + ". Position of car "+ (i+1) + " within 10 mts : " + distance[i]);
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isDriverLast(int carNo) {
        for(int i = 0; i< numCars; i++) {

            if (distance[i]<=distance[carNo]) {
                // Nitro not necessary
                return false;
            }
        }
        return true;
    }

    private void updateCompletionStats() {
        for(int i = 0; i< numCars; i++){
            if (!raceCompleted[i]) {
                if(distance[i]> trackLen) {
                    raceCompleted[i] = true;
                    finishingTime[i]= time;
                }
            }
        }
    }

    public static void main(String args[]) {
        FormulaOne formulaOne = new FormulaOne();
        formulaOne.computeFinalSpeedAndTime(2, 1000);
    }
}